export class KPI {
  constructor(
    public id: number,
    public name: string,
    public value?: number,
    public units?: string,
    public dynamic?: number,
    public disabled?: boolean
  ) {}
}
