import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { KPI } from './kpi';
import { KpiStoreService } from './kpi-store.service';

@Component({
  selector: 'app-kpi',
  templateUrl: './kpi.component.html',
  styleUrls: ['./kpi.component.sass']
})
export class KpiComponent implements OnInit {

  public currentKPISubject: BehaviorSubject<KPI | null> = new BehaviorSubject<KPI | null>(null);
  public fullList: KPI[] = [];
  public shortList: KPI[] = [];

  constructor(
    private kpiStore: KpiStoreService
  ) { }

  ngOnInit() {
    this.initSubscriptions();
  }

  public get currentKPI() {
    return this.currentKPISubject.getValue() || null;
  }

  public selectKPI(id: number): void {
    this.currentKPISubject.next(this.fullList.filter((item) => item.id === id)[0]);
  }

  public editItem(value: KPI) {
    if (value.id !== undefined) {
      this.kpiStore.setItem(value).subscribe();
    }
  }

  private initSubscriptions(): void {
    this.kpiStore.list.subscribe((res) => {
      this.fullList = res;
      this.shortList = res.slice(0, 3);
    });
  }

}
