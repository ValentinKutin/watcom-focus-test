export function parseKpiValue(value: string): { value: number, units: string } | null {
  const VALUE_REGEXP = /^[0-9\s]+/i;

  if (!value) {
    return null;
  }

  if (value.search(VALUE_REGEXP) !== 0) {
    return null;
  }

  const number: string[] = value.match(VALUE_REGEXP);
  const text: string = value.replace(VALUE_REGEXP, '');

  if (!number && number.length > 1) {
    return null;
  }

  return {
    value: parseInt(number[0].replace(/\s/ig, ''), 10),
    units: text
  };
}
