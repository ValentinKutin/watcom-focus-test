import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { AbstractControl, FormControl, FormGroup } from '@angular/forms';

import { KPI } from '../kpi';
import { parseKpiValue} from './kpi-form';

export function parseValue(value: string): { value: number, units: string } | null {
  const VALUE_REGEXP = /^[0-9\s]+/i;

  if (!value) {
    return null;
  }

  if (value.search(VALUE_REGEXP) !== 0) {
    return null;
  }

  const number: string[] = value.match(VALUE_REGEXP);
  const text: string = value.replace(VALUE_REGEXP, '');

  if (!number && number.length > 1) {
    return null;
  }

  return {
    value: parseInt(number[0].replace(/\s/ig, ''), 10),
    units: text
  };
}

@Component({
  selector: 'app-kpi-form',
  templateUrl: './kpi-form.component.html',
  styleUrls: ['./kpi-form.component.sass']
})
export class KpiFormComponent implements OnInit, OnChanges {

  @Input() private data: KPI | null;
  @Output() private updateData = new EventEmitter<KPI>();

  public readonly form = new FormGroup({
    name: new FormControl(''),
    value: new FormControl('', this.validateKpiValue)
  });

  constructor() {}

  ngOnInit() {
    this.form.valueChanges.subscribe((res) => {
      if (this.form.valid) {
        const { value, units } = parseValue(res.value);

        if (value === undefined || units === undefined) {
          return;
        }

        this.updateData.emit({
          ...this.data,
          name: res.name,
          value: value,
          units: units
        });
      }
    });
  }

  ngOnChanges(changes) {
    const data = changes.data;

    if (data) {
      const currentValue = data.currentValue;

      if (!currentValue) {
        return;
      }

      this.form.controls.name.setValue(currentValue.name);
      this.form.controls.value.setValue(`${data.currentValue.value} ${data.currentValue.units}`);
    }
  }

  private validateKpiValue(control: AbstractControl) {
    const result = parseKpiValue(control.value);

    if (result === null) {
      return { validateKpiValue: true };
    }

    return null;
  }

}
