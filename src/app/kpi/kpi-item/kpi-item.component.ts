import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { KPI } from '../kpi';

@Component({
  selector: 'app-kpi-item',
  templateUrl: './kpi-item.component.html',
  styleUrls: ['./kpi-item.component.sass']
})
export class KpiItemComponent implements OnInit {
  @Input() public data: KPI;
  @Input() public active = false;

  @Output() private select = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
  }

  onClick(): void {
    if (!this.data.disabled) {
      this.select.emit(this.data.id);
    }
  }

}
