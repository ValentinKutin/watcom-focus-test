import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject, throwError } from 'rxjs';
import { catchError, debounceTime } from 'rxjs/operators';

import { KPI } from './kpi';

@Injectable({
  providedIn: 'root'
})
export class KpiBackendService {

  private readonly _setItemSubject: Subject<KPI> = new Subject();
  private readonly _setItemSubjectQuery: Subject<KPI> = new Subject();

  constructor(
    private http: HttpClient
  ) {
    this.initSubscriptions();
  }

  public get list(): Observable<KPI[]> {
    return this.http.get<KPI[]>('http://localhost:3000/kpi')
      .pipe(catchError((err => {
        return throwError(err);
      })));
  }

  public setItem(element: KPI): Observable<KPI> {
    this._setItemSubject.next(element);

    return new Observable((observer) => {
      this._setItemSubjectQuery.subscribe(
        (res) => {
          observer.next(res);
          observer.complete();
        },
        (err) => {
          observer.error(err);
        }
      );
    });
  }

  private initSubscriptions(): void {
    this._setItemSubject
      .pipe(debounceTime(2000))
      .subscribe((res) => {
        this.http.put<KPI>(`http://localhost:3000/kpi/${res.id}`, res)
          .subscribe(
            () => {
              this._setItemSubjectQuery.next(res);
            },
            (err) => {
              this._setItemSubjectQuery.error(err);
            }
          );
      });
  }
}
