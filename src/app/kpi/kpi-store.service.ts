import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

import { KPI } from './kpi';
import { KpiBackendService } from './kpi-backend.service';

@Injectable({
  providedIn: 'root'
})
export class KpiStoreService {

  private readonly _list: BehaviorSubject<KPI[]> = new BehaviorSubject([]);

  constructor(
    private kpiBackend: KpiBackendService
  ) { }

  public get list(): Observable<KPI[]> {
    if (this._list.getValue().length === 0) {
      this.kpiBackend.list.subscribe((res) => {
        this._list.next(res);
      });
    }
    return this._list;
  }

  public setItem(item: KPI): Observable<KPI> {
    const newList = this._list.getValue().map((item2) => {
      if (item.id === item2.id) {
        return item;
      }

      return item2;
    });

    this._list.next(newList);

    return this.kpiBackend.setItem(item);
  }
}
