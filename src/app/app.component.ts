import { Component, OnInit } from '@angular/core';
import { of } from 'rxjs';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
  public isLoaded = false;

  ngOnInit() {
    of(true).pipe(delay(5000)).subscribe(() => {
      this.isLoaded = true;
    });
  }
}
