import { NgModule } from '@angular/core';

import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { KpiComponent } from './kpi/kpi.component';
import { KpiItemComponent } from './kpi/kpi-item/kpi-item.component';
import { KpiFormComponent } from './kpi/kpi-form/kpi-form.component';

@NgModule({
  declarations: [
    AppComponent,
    KpiItemComponent,
    KpiComponent,
    KpiFormComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [ ],
  bootstrap: [ AppComponent ]
})
export class AppModule {}
